//
//  SongListViewController.swift
//  Altimetrik_Demo
//
//  Created by MANISH_iOS on 11/08/17.
//  Copyright © 2017 iDev. All rights reserved.
//

import UIKit

class SongListViewController: MasterViewController
{

    // MARK:- IBOutlets
    @IBOutlet weak var songListTableView: UITableView!
    
    // Variables
    let refreshControlColl = UIRefreshControl()

    override func viewDidLoad() {
        shouldHideNavBarLeftBtn = true // This will hide the back button
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
        preSetup()
    }
    
    // Setting up before view did appear
    private func preSetup()
    {
        preUI()
        getUpdatedData()
    }
    // Pre user interface changes
    private func preUI()
    {
        self.title = "iTunes Top 50 Songs"
        tableViewPreSetUp()
        addRefreshControl()
    }
    private func tableViewPreSetUp()
    {
        songListTableView .register(UINib(nibName: "SongTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.identifiersED.songTableViewCellID)
        songListTableView.rowHeight = Constants.screenSize.screenHeight * 0.2
        self.automaticallyAdjustsScrollViewInsets = false
    }
    /*!
     Adding refresh control for updating data on pull to refresh
     */
    private func addRefreshControl()
    {
        // Adding Refresh Control
        refreshControlColl.attributedTitle = refreshControlColl.pullToRefreshAttributedStringWithColor(color: Constants.customColors.blackColorBase, title: "Fetching Categories...")
        
        refreshControlColl.addTarget(self, action: #selector(SongListViewController.getUpdatedData), for: .valueChanged)
        
        refreshControlColl.tintColor = Constants.customColors.blackColorBase
        // Add to Table View
        if #available(iOS 10.0, *) {
            songListTableView.refreshControl = refreshControlColl
        } else {
            songListTableView.addSubview(refreshControlColl) // not required when using UITableViewController
        }
    }

    func getUpdatedData()
    {
        Webservices.shared.populateSongs(target: self) { 
            self.songListTableView.reloadData()
            self.refreshControlColl.endRefreshing()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension SongListViewController: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return Constants.topSongsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : SongTableViewCell = tableView.dequeueReusableCell(withIdentifier: Constants.identifiersED.songTableViewCellID, for: indexPath) as! SongTableViewCell
        let song    = Constants.topSongsArray[indexPath.row]
        cell.songTitleLabel.text = song.songName?.uppercaseFirst
        cell.songDisplayImage.setImageKingfisher(urlStr: song.imageURLSong!)
        cell.songBackgroundImage.setImageKingfisher(urlStr: song.imageURLSong!)
        cell.customResizeFix()
        return cell
    }
}
