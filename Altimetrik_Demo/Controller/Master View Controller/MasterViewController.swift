//
//  MasterViewController.swift
//  Altimetrik_Demo
//
//  Created by Manish on 8/11/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import UIKit

// This class is a master view controller class which is going to be a parent class of all the required view controllers, also it will manage the Autolayout hotspot UI distortion issue
class MasterViewController: UIViewController
{
    
    var mBackgroundColor : UIColor!
    var shouldHideNavBarLeftBtn : Bool!
    var statusHeight : CGFloat!
    let screenHeight : CGFloat = UIScreen.main.bounds.height
    var viewOriginY : CGFloat!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if shouldHideNavBarLeftBtn == nil
        {
            addCustomBackButton()
        }
        preParentUI()
        addNCObservers()
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        removeNCObservers()
    }
    
    func preParentUI()
    {
        if mBackgroundColor == nil
        {
           mBackgroundColor = Constants.customColors.whiteColorBase
        }
        view.backgroundColor = mBackgroundColor
        self.myBasicNavBar()
    }
    
    
    func addCustomBackButton()
    {
        let backBtn = UIBarButtonItem()
        let btnImage: UIImage = #imageLiteral(resourceName: "backArrowWhiteFilled")
        let renderedImage = btnImage.withRenderingMode(.alwaysOriginal)
        backBtn.image = renderedImage
        backBtn.action = #selector(popSelf)
        backBtn.target = self
        navigationItem.leftBarButtonItem = backBtn
    }
    
    func popSelf()
    {
        let _ = navigationController?.popViewController(animated: true)
    }

    /**
     Set change status bar size Observer.
     */
    func addNCObservers()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(MasterViewController.statusBarFrameWillChanged(notification:)), name: NSNotification.Name.UIApplicationWillChangeStatusBarFrame, object: nil)
    }
    
    /**
     Remove change status bar size Observer.
     */
    func removeNCObservers()
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillChangeStatusBarFrame, object: nil)
    }
    
    func statusBarFrameWillChanged(notification: NotificationCenter)
    {
        statusHeight = UIApplication.shared.statusBarFrame.height
        var viewHeight = screenHeight - statusHeight + 44
        
        if statusHeight > 20
        {
            viewOriginY = -20
            viewHeight = screenHeight - statusHeight + 44
        }else {
            viewOriginY = 0
            viewHeight = screenHeight - statusHeight + 20
        }
        
        UIView.animate(withDuration: 0.30) {
            self.view.frame = CGRect(x: 0, y: self.viewOriginY, width: self.view.frame.size.width, height: viewHeight)
        }
    }
    
}
