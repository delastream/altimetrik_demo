//
//  Parser.swift
//  Altimetrik_Demo
//
//  Created by Manish on 8/11/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import Foundation

class Parser: NSObject
{
    
    /// It will check if our request is successful or not
    ///
    /// - Parameter responseObject: response object of api hit
    /// - Returns: true if success, false if failed
    class func parserCheckIfSuccess(responseObject :  NSDictionary) -> Bool
    {
        if let _ =  responseObject.value(forKey: "feed") as? NSDictionary
        {
            return true
        }else
        {
            return false
        }
    }
    
    /// It will check if our request is successful or not
    ///
    /// - Parameter responseObject: response object of api hit
    /// - Returns: true if success, false if failed
    class func parserGetSongsArray(responseObject :  NSDictionary) -> [NSDictionary]!
    {

        if let feed  = responseObject.value(forKey: "feed") as? NSDictionary, let entry  = feed.value(forKey: "entry") as? [NSDictionary]!
        {
            return entry
            
        }else
        {
            return nil
        }
    }

    class func parseCategoriesResponse(songs :  [NSDictionary]) -> [TopSongModel]
    {
        var songModels = [TopSongModel]()
        
        for song in songs
        {
            let songData = song as AnyObject
            let songModel = TopSongModel()
            
            
            if let nameDic =  songData.value(forKey: "im:name") as? NSDictionary, let name  = nameDic.value(forKey: "label") as? String
            {
                songModel.songName = name
            }
            if let imgArray =  songData.value(forKey: "im:image") as? NSArray, imgArray.count > 0, let imgLink  = (imgArray[0] as! NSDictionary).value(forKey: "label") as? String
            {
                songModel.imageURLSong = imgLink
            }
            if let releaseDateDic =  songData.value(forKey: "im:releaseDate") as? NSDictionary, let attributeDate  = releaseDateDic.value(forKey: "attributes") as? NSDictionary, let labelDate  = attributeDate.value(forKey: "label") as? String
            {
                songModel.releaseDate = labelDate
            }
            if let priceDic =  songData.value(forKey: "im:price") as? NSDictionary, let attributePrice  = priceDic.value(forKey: "label") as? String
            {
                songModel.songPrice = attributePrice
            }
            if let titleDic =  songData.value(forKey: "title") as? NSDictionary, let title  = titleDic.value(forKey: "label") as? String
            {
                songModel.songTitle = title
            }

            songModels.append(songModel)
        }

        return songModels
    }
}
