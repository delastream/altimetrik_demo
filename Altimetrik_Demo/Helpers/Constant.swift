//
//  Constant.swift
//  Altimetrik_Demo
//
//  Created by Manish on 8/11/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import Foundation
import UIKit


struct Constants
{
    /// Base server URL and Image URL
    struct apis
    {
        static let baseServer = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/"
    }
    
    
    /// List of all the service name which we are using in this project
    struct serviceName
    {
        static let getTopSongs           = "topsongs/limit=50/json"
    }
    
    /// It is containing all the colors which we are using in this project
    struct customColors
    {
        static let whiteColorBase             = UIColor.init(hexString: "#EDEDED")
        static let tabColorBase               = UIColor.init(hexString: "#F7F7F7")
        static let blackColorBase             = UIColor.init(hexString: "#343434")
        static let redColorBase               = UIColor.init(hexString: "#B4303E")
        static let grayColorBase              = UIColor.init(hexString: "#8B8B8B")

    }
    
    /// It is containing all the fonts which we are using in this project
    struct customFonts
    {
        static let fontSizeSmall: CGFloat  = (UIDevice.current.userInterfaceIdiom == .pad) ?  15.0 : 12.0
        static let fontSizeMedium: CGFloat  = (UIDevice.current.userInterfaceIdiom == .pad) ?  18.0 : 15.0
        static let fontSizeLarge: CGFloat  = (UIDevice.current.userInterfaceIdiom == .pad) ?  20.0 : 17.0
        
        static let fontSizeTab: CGFloat  = (UIDevice.current.userInterfaceIdiom == .pad) ?  10.0 : 8.0

        
        static let sertigFont                   = UIFont(name: "sertig", size: fontSizeSmall)
        static let sertigFontMedium             = UIFont(name: "sertig", size: fontSizeMedium)
        static let sertigFontTitle              = UIFont(name: "sertig", size: fontSizeLarge)
        
        static let sertigFontTab                   = UIFont(name: "sertig", size: fontSizeTab)
        
    }
    /// It will check the screen size and return a static variable with proper and calculated result
    struct screenSize
    {
        static let screenWidth  = UIScreen.main.bounds.size.width
        static let screenHeight = UIScreen.main.bounds.size.height
    }
    
    struct identifiersED
    {
        // View Controllers
        static let songListVCID                 = "songListVCID"
        static let songDetailsVCID              = "songDetailsVCID"
        
        // Custom Cells
        static let songTableViewCellID      = "songTableViewCellID"
    }
    
    // Constants
    static let mainAppName : String = "Altimetrix"
    static var topSongsArray: [TopSongModel] = [TopSongModel]()
}
