//
//  Presenter.swift
//  Altimetrik_Demo
//
//  Created by Manish on 8/11/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import Foundation
import UIKit

class Presenter
{
    static let shared = Presenter()
    let mainStoryBoard          = UIStoryboard(name: "Main", bundle: nil)
    var landingPageVCObj    : SongListViewController!
    var landingPageNVCObj   : UINavigationController!

    //MARK:- Get View Controllers
    
    /// To get Landing Page View Controller
    ///
    /// - Returns: ViewController
    func getLandinPageVC() -> SongListViewController
    {
        if landingPageVCObj == nil
        {
            landingPageVCObj = mainStoryBoard.instantiateViewController(withIdentifier: Constants.identifiersED.songListVCID) as! SongListViewController
        }
        return landingPageVCObj
    }
    func getLandinPageNVC() -> UINavigationController
    {
        if landingPageNVCObj == nil
        {
            landingPageVCObj = mainStoryBoard.instantiateViewController(withIdentifier: Constants.identifiersED.songListVCID) as! SongListViewController
        }
        if landingPageNVCObj == nil
        {
            landingPageNVCObj = UINavigationController(rootViewController: landingPageVCObj)
        }

        return landingPageNVCObj
    }
}
