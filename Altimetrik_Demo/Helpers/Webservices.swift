//
//  Webservices.swift
//  Altimetrik_Demo
//
//  Created by Manish on 8/11/17.
//  Copyright © 2017 iDevelopers. All rights reserved.
//

import Foundation
import Alamofire


/// This is a webservice singleton classed, any class required to hit a web api can use this helper class
class Webservices: NSObject
{
    static let shared = Webservices()
    let sharedApp  = UIApplication.shared
    
    
    override init()
    {
        super.init()
    }
    /*!
     Get request global
     
     - parameter serviceName:       Name of service
     - parameter parameters:        Parameters to Pass
     - parameter showLoader:        If you want to show the Activity Indicator
     - parameter completionHandler: Will return response
     */
    func getRequest(_ serviceName : String, parameters : [String : Any]!,  showLoader : Bool, passedView : UIView?, passedText : String?, withCustomImage: Bool = true, shouldAuthorize : Bool, completionHandler :@escaping  (_ response : AnyObject?, _ status : Bool, _ error : String?, _ time : Timeline?) -> Void)
    {
        if showLoader == true
        {
            MPLoader.shared.showLoader(passedText != nil ? passedText! : "Loading...", blockUI: true, passedView: passedView != nil ? passedView! : nil, withCustomImage: false)
        }
        
        guard let url = URL(string: Constants.apis.baseServer + serviceName) else {
            if showLoader == true
            {
                MPLoader.shared.hideLoader()
            }
                
            completionHandler(nil, false, "Error: cannot create URL", nil)
            return
        }
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30 // To Set Up TimeOut
        
        var headers: HTTPHeaders? = nil
        
         if shouldAuthorize == true
         {
             headers = [
             "Authorization": "Bearer \(4)",
             "Accept": "application/json"
             ]
         }
        
        manager.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
            .downloadProgress { progress in

            }
            .validate { request, response, data in
                // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                return .success
            }
            .responseJSON { response in
                
                if showLoader == true
                {
                    MPLoader.shared.hideLoader() // If Loader was assigned hide the loader
                }
                
                guard response.result.error == nil else
                {
                    // got an error in getting the data, need to handle it
                    completionHandler(nil, false,  (response.result.error?.localizedDescription)!,  response.timeline)
                    return
                }
                
                if let value = response.result.value
                {
                    completionHandler(value as AnyObject?, true, nil, response.timeline)
                    
                }else
                {
                    completionHandler(nil, false, (response.result.error?.localizedDescription)!, response.timeline)
                }
        }
    }
    
    //MARK:- Get Categories
    func populateSongs(target : UIViewController, completionHandler :@escaping  (
        ) -> Void)
    {
        Webservices.shared.getRequest(Constants.serviceName.getTopSongs, parameters: nil, showLoader: true, passedView: nil, passedText: "Getting Songs....", shouldAuthorize: false)
        { (responseObject, successStatus, errorMessage, timeline) in
            
            if responseObject != nil, successStatus == true
            {
                if Parser.parserCheckIfSuccess(responseObject: responseObject as! NSDictionary) == true, let songs = Parser.parserGetSongsArray(responseObject: responseObject as! NSDictionary)
                {
                    Constants.topSongsArray = Parser.parseCategoriesResponse(songs: songs)
                    completionHandler()
                }else
                {
                    target.showBasicAlert(title: "Failed", message: "Unable to get data, please try again later.")
                    completionHandler()
                }
            }else
            {
                target.showBasicAlert(title: "Failed", message: errorMessage != nil ? errorMessage! : "Unable to get data, please try again later.")
                completionHandler()
            }
        }
    }
}
