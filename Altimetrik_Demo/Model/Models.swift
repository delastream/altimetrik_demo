//
//  Models.swift
//  Altimetrik_Demo
//
//  Created by MANISH_iOS on 11/08/17.
//  Copyright © 2017 iDev. All rights reserved.
//

import Foundation

/// Model class for song details, songName -> Name of the song | imageURLSong -> Thumbnail image url of the song's artwork | songTitle -> Title of the song | songPrice -> Cost of the song | releaseDate -> Release date of the song
class TopSongModel: NSObject
{
    override init()
    {
        super.init()
    }
    var songName: String?           = "N/A"
    var imageURLSong: String?
    var songTitle: String?          = "N/A"
    var songPrice: String?          = "N/A"
    var releaseDate: String?        = "N/A"
}
