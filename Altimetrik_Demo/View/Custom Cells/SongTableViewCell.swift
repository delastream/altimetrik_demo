//
//  SongTableViewCell.swift
//  Altimetrik_Demo
//
//  Created by MANISH_iOS on 11/08/17.
//  Copyright © 2017 iDev. All rights reserved.
//

import UIKit

class SongTableViewCell: UITableViewCell {

    @IBOutlet var songTitleLabel: UILabel!
    @IBOutlet var songDisplayImage: UIImageView!
    @IBOutlet var songBackgroundImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        preUI()
    }
    func preUI()
    {
        songDisplayImage.applyBorderToView(borderColor: Constants.customColors.whiteColorBase, width: 1.0)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
